package model;

import models.Medicao;

public class MedicaoGenerica
  extends Medicao
{
  public String chuva = null;
  public String chuva_acumulada = null;
  public String nivel = null;
  public String vazao = null;
  public String curva_id = null;

  
  public String getChuva() { return this.chuva; }

  
  public void setChuva(String chuva) { this.chuva = chuva; }

  
  public String getChuva_acumulada() { return this.chuva_acumulada; }

  
  public void setChuva_acumulada(String chuva_acumulada) { this.chuva_acumulada = chuva_acumulada; }

  
  public String getNivel() { return this.nivel; }

  
  public void setNivel(String nivel) { this.nivel = nivel; }

  
  public String getVazao() { return this.vazao; }

  
  public void setVazao(String vazao) { this.vazao = vazao; }

  
  public String getCurva_id() { return this.curva_id; }

  
  public void setCurva_id(String curva_id) { this.curva_id = curva_id; }
}
