package classes;

public class Usuario
{
  private int id;
  private String nome;
  private String username;
  private String email;
  
  public int getId() { return this.id; }

  
  public void setId(int id) { this.id = id; }

  
  public String getNome() { return this.nome; }

  
  public void setNome(String nome) { this.nome = nome; }

  
  public String getUsername() { return this.username; }

  
  public void setUsername(String username) { this.username = username; }

  
  public String getEmail() { return this.email; }

  
  public void setEmail(String email) { this.email = email; }
}
