package classes;

public class Encarregado
  extends Usuario
{
  private int encarregado_id;
  private int supervisor_id;
  
  public int getEncarregado_id() { return this.encarregado_id; }

  
  public void setEncarregado_id(int encarregado_id) { this.encarregado_id = encarregado_id; }

  
  public int getSupervisor_id() { return this.supervisor_id; }

  
  public void setSupervisor_id(int supervisor_id) { this.supervisor_id = supervisor_id; }
}
