package database;

import classes.Encarregado;
import execution.Main;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import model.MedicaoGenerica;
import models.Medicao;
import org.postgresql.util.PSQLException;







public class Database
{
  private Connection conectar() {
    String driver = "org.postgresql.Driver";
    String user = "postgres";
    String senha = "TC0ntr0l";
    String url = "jdbc:postgresql://143.107.108.110:5432/hidrologia";
    
    try {
      Class.forName(driver);
      return DriverManager.getConnection(url, user, senha);
    }
    catch (ClassNotFoundException ex) {
      System.err.print(ex.getMessage());
    } catch (SQLException e) {
      System.err.print(e.getMessage());
    } 
    return null;
  }

  
  public HashMap<String, Integer> obterPostosTelefonados() {
    HashMap<String, Integer> aux = new HashMap<String, Integer>();
    
    String sql = "select * from postos where tipo_de_posto_id = 2";
    Statement stmt = null;
    
    Connection connection = conectar();
    
    if (connection != null) {
      try {
        stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        
        while (rs.next()) {
          aux.put(rs.getString("prefixo"), Integer.valueOf(rs.getInt("id")));
        }
        
        return aux;
      }
      catch (Exception e) {
        System.err.println(e);
      } finally {
        fecharConec(connection);
      } 
    }

    
    return null;
  }


  
  public HashMap<String, Integer> obterPostosPorTipo(int tipo) {
    HashMap<String, Integer> aux = new HashMap<String, Integer>();
    
    String sql = "select * from postos where tipo_de_posto_id = " + tipo;
    Statement stmt = null;
    
    Connection connection = conectar();
    
    if (connection != null) {
      try {
        stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        
        while (rs.next()) {
          aux.put(rs.getString("prefixo"), Integer.valueOf(rs.getInt("id")));
        }
        
        return aux;
      }
      catch (Exception e) {
        System.err.println(e);
      } finally {
        fecharConec(connection);
      } 
    }

    
    return null;
  }




  
  public void inserirMedicoes() {
    Connection connection = conectar();
    
    List<String> keys = new ArrayList<String>(Main.MEDICOES.keySet());
    Collections.sort(keys);
    
    for (String posto : keys) {
      
      System.out.println("Medicoes do Posto == > " + posto);
      List<Medicao> medicoes = (List)Main.MEDICOES.get(posto);
      
      if (connection != null) {
        for (Medicao medicao : medicoes) {
          
          try {
            PreparedStatement ps = connection.prepareStatement("insert into medicoes(posto_id, data_hora, valor_leitura, tipo_transmissao_id, origem_da_informacao, tipo_classificacao_medicao_id) values(?,?,?,?,?,?)");







            
            Timestamp timestamp = null;
            
            try {
              SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
              
              Calendar c = Calendar.getInstance();
              c.setTime(dateFormat.parse(medicao.getData_hora()));





              
              timestamp = new Timestamp(c.getTimeInMillis());
            
            }
            catch (Exception e) {
              System.err.println(e);
            } 
            
            ps.setInt(1, medicao.getPostoId());
            ps.setTimestamp(2, timestamp);
            
            if (medicao.getValor_leitura() != null) {
              ps.setFloat(3, Float.parseFloat(medicao.getValor_leitura()));
            } else {
              ps.setNull(3, 6);
            } 
            
            ps.setInt(4, medicao.getTipo_transmissao_id());
            ps.setString(5, "testando");
            ps.setInt(6, medicao.getTipo_classificacao_medicao_id());
            
            if (ps.executeUpdate() > 0) {
              System.out.println(String.valueOf(medicao.getData_hora()) + " - " + medicao.getValor_leitura());
            }
            
            ps.close();
          }
          catch (PSQLException e) {
            if (!e.getMessage().contains("ERROR: duplicate key value violates unique constraint"))
            {
              
              System.out.println("Error code ao enviar para sibh --> " + e.getMessage());
            
            }
          }
          catch (SQLException e) {
            System.err.println(e);
          } 
        } 
        
        continue;
      } 
      System.out.println("SEM CONEXAO");
    } 


    
    fecharConec(connection);
  }



  
  public void inserirMedicoesCemaden() {
    Connection connection = conectar();
    
    List<String> keys = new ArrayList<String>(Main.MEDICOES_CEMADEN_PLU.keySet());
    Collections.sort(keys);
    
    int postos_sincronizados = 0;
    int qtd_total_postos = Main.MEDICOES_CEMADEN_PLU.size();
    
    for (String posto : keys) {
      
      postos_sincronizados++;
      int sincronizadas = 0;
      List<MedicaoGenerica> medicoes = Main.MEDICOES_CEMADEN_PLU.get(posto);
      Main.MEDICOES_CEMADEN_PLU.remove(posto);
      System.out.println("Medicoes do Posto == > " + posto + " - " + medicoes.size() + " Medi����es para sincronizar (" + postos_sincronizados + "/" + qtd_total_postos + ")");
      if (connection != null) {
        for (MedicaoGenerica medicao : medicoes) {
          
          try {
            PreparedStatement ps = connection.prepareStatement("insert into dados_temp_loggers(prefixo, data_hora, chuva_evento, chuva_acumulada, nivel, vazao, tensao_bateria, tipo_transmissao_id, origem_da_informacao, sync_status) values(?,?,?,?,?,?,?,?,?,?)");



            
            medicao.setData_hora(definirGMT(medicao.getData_hora()));
            
            Timestamp timestamp = null;
            
            try {
              SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
              Date dataparse = dateFormat.parse(medicao.getData_hora());
              timestamp = new Timestamp(dataparse.getTime());
            } catch (Exception e) {
              System.err.println(e);
            } 
            
            ps.setString(1, medicao.getPrefixo_do_posto());
            ps.setTimestamp(2, timestamp);
            
            if (medicao.getChuva() != null) {
              float valor = Float.parseFloat(medicao.getChuva());
              ps.setFloat(3, Float.parseFloat(String.format("%.2f", new Object[] { Float.valueOf(valor) }).replaceAll(",", ".")));
            } else {
              ps.setNull(3, 6);
            } 
            
            if (medicao.getChuva_acumulada() != null) {
              float valor = Float.parseFloat(medicao.getChuva_acumulada());
              ps.setFloat(4, Float.parseFloat(String.format("%.2f", new Object[] { Float.valueOf(valor) }).replaceAll(",", ".")));
            } else {
              ps.setNull(4, 6);
            } 
            if (medicao.getNivel() != null) {
              float valor = Float.parseFloat(medicao.getNivel());
              ps.setFloat(5, Float.parseFloat(String.format("%.3f", new Object[] { Float.valueOf(valor) }).replaceAll(",", ".")));
            } else {
              ps.setNull(5, 6);
            } 
            if (medicao.getVazao() != null) {
              float valor = Float.parseFloat(medicao.getVazao());
              ps.setFloat(6, Float.parseFloat(String.format("%.3f", new Object[] { Float.valueOf(valor) }).replaceAll(",", ".")));
            } else {
              ps.setNull(6, 6);
            } 
            
            ps.setInt(7, 18);
            ps.setInt(8, 4);
            ps.setString(9, "WSJAVA-CEMADEN");
            ps.setInt(10, 0);
            
            if (ps.executeUpdate() > 0)
            {
              sincronizadas++;
            }
            
            ps.close();
          }
          catch (PSQLException e) {
            if (!e.getMessage().contains("ERROR: duplicate key value violates unique constraint"))
            {
              
              System.out.println("Error code ao enviar para sibh --> " + e.getMessage());
            
            }
          }
          catch (SQLException e) {
            System.err.println(e);
          } 
        } 

        
        System.out.println("Medi����es sincronizadas ==> " + sincronizadas);
        continue;
      } 
      System.out.println("SEM CONEXAO");
    } 




    
    fecharConec(connection);
  }



  
  public void inserirMedicoesProdesp(String tipo) {
    Connection connection = conectar();
    
    List<String> keys = new ArrayList<String>(Main.MEDICOES_PRODESP.keySet());
    Collections.sort(keys);
    
    for (String posto : keys) {



      
      if (connection != null) {
        
        for (String[] dados : Main.relacao_postos_prodesp) {
          if (dados[0].equals(posto)) {
            
            String prefixo_sibh = dados[1];
            System.out.println("POSTO ==> " + prefixo_sibh);
            if (Main.POSTOS.containsKey(prefixo_sibh)) {
              
              int posto_id = ((Integer)Main.POSTOS.get(prefixo_sibh)).intValue();
              
              String ano = null;
              String mes = null;

              
              HashMap<String, List<String>> aux = (HashMap)Main.MEDICOES_PRODESP.get(posto);
              List<String> datas = new ArrayList<String>(aux.keySet());
              Collections.sort(datas);
              
              for (String ano_mes : datas) {
                List<String> json_valores = new ArrayList<String>();
                System.out.println("ANO MES" + ano_mes);
                ano = ano_mes.substring(0, 4);
                mes = ano_mes.substring(4, 6);

                
                String text = "";
                int i = 0;
                
                for (String valor : aux.get(ano_mes)) {
                  if (valor.trim().equals("9.999") || valor.trim().equals("99.999,999")) {
                    text = String.valueOf(text) + "-";
                  } else {
                    text = String.valueOf(text) + valor.trim().replace(",", ".");
                  } 
                  i++;
                  if (i > 2) {
                    i = 0;
                    json_valores.add(text);
                    text = ""; continue;
                  } 
                  text = String.valueOf(text) + "|";
                } 






                
                try {
                  PreparedStatement ps = connection.prepareStatement("insert into dados_historicos_" + tipo + "s" + 
                      "(posto_id," + 
                      " ano," + 
                      " mes," + 
                      " valores," + 
                      " created_at," + 
                      " updated_at)" + 
                      " values(?,?,?,?,now(),now())");
                  Array array = null;
                  if (tipo == "plu") {
                    array = connection.createArrayOf("text", ((List)aux.get(ano_mes)).toArray());
                  } else {
                    array = connection.createArrayOf("text", json_valores.toArray());
                  } 
                  ps.setInt(1, posto_id);
                  ps.setString(2, ano);
                  ps.setString(3, mes);
                  ps.setArray(4, array);

                  
                  if (ps.executeUpdate() > 0) {
                    System.out.println(String.valueOf(prefixo_sibh) + " - " + posto_id + " - " + ano_mes);
                  }
                  
                  ps.close();
                }
                catch (PSQLException e) {
                  
                  if (!e.getMessage().contains("ERROR: duplicate key value violates unique constraint"))
                  {
                    
                    System.out.println("Error code ao enviar para sibh --> " + e.getMessage());
                  
                  }
                }
                catch (SQLException e) {
                  System.err.println(e);
                } 
              } 
              
              continue;
            } 
            System.out.println("Prefixo " + prefixo_sibh + " n��o cadastrado na base");
          } 
        } 

        
        continue;
      } 

      
      System.out.println("SEM CONEXAO");
    } 


    
    fecharConec(connection);
  }





  
  public List<Encarregado> getEncarregados() {
    List<Encarregado> encarregados = new ArrayList<Encarregado>();
    
    String sql = "select encarregados.id as id_encarregado, encarregados.supervisor_id as id_supervisor, usuarios.* from usuarios join encarregados on (usuarios.id = encarregados.usuario_id)";
    Statement stmt = null;
    
    Connection connection = conectar();
    
    if (connection != null) {
      try {
        stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        
        while (rs.next()) {
          Encarregado encarregado = new Encarregado();
          encarregado.setNome(rs.getString("nome"));
          encarregado.setUsername(rs.getString("username"));
          encarregado.setEmail(rs.getString("email"));
          encarregado.setId(rs.getInt("id"));
          encarregado.setSupervisor_id(rs.getInt("id_supervisor"));
          encarregado.setEncarregado_id(rs.getInt("id_encarregado"));
          encarregados.add(encarregado);
        } 
        
        return encarregados;
      }
      catch (Exception e) {
        System.err.println(e);
      } finally {
        fecharConec(connection);
      } 
    }

    
    return null;
  }



  
  private String definirGMT(String data_hora) {
    String ano = data_hora.substring(0, 4);
    String mes = data_hora.substring(5, 7);
    String dia = data_hora.substring(8, 10);
    int hora = Integer.parseInt(data_hora.substring(11, 13));
    hora += 3;
    String hora_gmt = String.format("%02d", new Object[] { Integer.valueOf(hora) });
    String minutos = data_hora.substring(14, 16);
    
    return new String(String.valueOf(ano) + "-" + mes + "-" + dia + " " + hora_gmt + ":" + minutos);
  }


  
  private void fecharConec(Connection connection) {
    try {
      connection.close();
    } catch (SQLException e) {
      
      System.err.println("Erro ao fechar conex��o ==> " + e);
    } 
  }
}
