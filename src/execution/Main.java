package execution;

import controller.UploaderController;
import database.Database;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import model.MedicaoGenerica;
import models.Medicao;



public class Main
{
  public static HashMap<String, HashMap<String, List<Float>>> MEDICOES_ARQUIVO = new HashMap();
  
  public static HashMap<String, List<Medicao>> MEDICOES = new HashMap();
  public static HashMap<String, Integer> POSTOS = new HashMap();
  public static List<String> PREFIXOS_SEM_ID = new ArrayList();
  public static HashMap<String, List<MedicaoGenerica>> MEDICOES_CEMADEN_PLU = new HashMap();
  public static HashMap<String, HashMap<String, List<String>>> MEDICOES_PRODESP = new HashMap();
  
  public static List<String[]> relacao_postos_prodesp = new ArrayList();
  
  public static Scanner s = new Scanner(System.in);

  
  public static void main(String[] args) {
    int opcao = getOpcao();
    
    if (opcao == 1) {
      leitorTransmissaoTelefonada();
    } else if (opcao == 2) {
      leitorTransmissaoTelefonadaFlu();
    } else if (opcao == 3) {
      leitorDadosCemadenPlu();
    } else if (opcao == 4) {
      leitorDadosProdespFlu();
    } else if (opcao == 5) {
      leitorDadosProdespPlu();
    } else if (opcao == 6) {
      atualizarEncarregadoPostos();
    } else {
      System.out.println("Opção Invalida");
    } 
  }

  
  public static void leitorTransmissaoTelefonada() {
    s.close();
    String url = "/home/diego/Área de Trabalho/Transmissao_Telefonada_Plu.csv";
    
    System.out.println("Start leitura do arquivo");
    UploaderController uploader = new UploaderController();
    
    uploader.uploadFileTelefonada(url, "PLU");
    System.out.println("Leitura completa");
    
    Database database = new Database();
    POSTOS = database.obterPostosTelefonados();
    
    uploader.criarMedicoes();

    
    System.out.println("Inserindo medições");
    database.inserirMedicoes();
  }
  
  public static void leitorTransmissaoTelefonadaFlu() {
    System.out.println("Op����o 'Leitor Dados Transmiss��o Telefonada FLU' Selecionada");
    
    s.close();
    String url = "/home/diego/Área de Trabalho/Transmiss��o_Telefonada_Flu.csv";
    
    System.out.println("Start leitura do arquivo");
    UploaderController uploader = new UploaderController();
    
    uploader.uploadFileTelefonada(url, "FLU");
    System.out.println("Leitura completa");
    
    Database database = new Database();
    POSTOS = database.obterPostosTelefonados();
    
    uploader.criarMedicoes();



    System.out.println("Inserindo medi����es");
    database.inserirMedicoes();
  }

  
  private static void leitorDadosCemadenPlu() {
    System.out.println("Op����o 'Leitor Dados Cemaden' Selecionada");
    
    System.out.print("Digite o Nome do Arquivo (Com Extens��o): ");
    String entrada = s.nextLine();
    
    s.close();
    
    UploaderController uploader = new UploaderController();
    System.out.println("Iniciando Leitura do Arquivo...");
    uploader.uploadFileCemadenPlu("/home/diegof/Desktop/Cemaden/SP_pluv_2018/" + entrada);
    System.out.println("Leitura completa e medi����es prontas!");
    
    System.out.println("Iniciando Processo de Sincroniza����o com base SIBH");
    
    Database database = new Database();
    database.inserirMedicoesCemaden();
    
    System.out.println("Processo finalizado!");
  }










  
  public static void leitorDadosProdespFlu() {
	  System.out.println("Processo inativo");
//    s.close();
//    
//    String url = "/home/diegof/Desktop/PRODESP_FLU.TXT";
//    String url_postos = "/home/diegof/Desktop/PRODESP_POSTOS.TXT";
//    
//    System.out.println("Obtendo postos FLU do SIBH");
//    Database database = new Database();
//    POSTOS = database.obterPostosPorTipo(1);
//    
//    System.out.println("Start leitura do arquivo");
//    UploaderController uploader = new UploaderController();
//    
//    uploader.ListaPostosProdesp(url_postos);
//    uploader.uploadFileProdespFlu(url);
//    
//    System.out.println("Leitura completa");
//    
//    database.inserirMedicoesProdesp("flu");
  }

  
  public static void leitorDadosProdespPlu() {
	  System.out.println("Processo inativo");
//    s.close();
//    
//    url = "/home/diegof/Desktop/PRODESP_PLU.TXT";
//    String url_postos = "/home/diegof/Desktop/PRODESP_POSTOS.TXT";
//    
//    System.out.println("Obtendo postos PLU do SIBH");
//    Database database = new Database();
//    POSTOS = database.obterPostosPorTipo(2);
//    
//    System.out.println("Start leitura do arquivo");
//    UploaderController uploader = new UploaderController();
//    
//    uploader.ListaPostosProdesp(url_postos);
//    uploader.uploadFileProdespPlu(url);
//    
//    System.out.println("Leitura completa");
//    
//    database.inserirMedicoesProdesp("plu");
  }



  
  public static void atualizarEncarregadoPostos() {
	  System.out.println("Processo inativo");
//    String url = "/home/diegof/Desktop/Rela����o_Postos_Encarregados.csv";
//    
//    UploaderController uploader = new UploaderController();
//    
//    uploader.lerArquivoEncarregados(url);
  }


  
  public static int getOpcao() {
    System.out.println("1 - Transmiss��o Telefonada PLU");
    System.out.println("2 - Transmiss��o Telefonada FLU");
    System.out.println("3 - Dados CEMADEN PLU ");
    System.out.println("4 - Dados PRODESP FLU ");
    System.out.println("5 - Dados PRODESP PLU ");
    System.out.println("6 - Atualizar Encarregados/Posto");
    System.out.println("Digite o n��mero correspondente a Op����o desejada: ");
    String entrada = s.nextLine();

    
    return Integer.parseInt(entrada);
  }
}
