package controller;

import execution.Main;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.TimeZone;
import model.MedicaoGenerica;
import models.Medicao;



public class UploaderController
{
  private enum CAMPOS
  {
    PREFIXO,
    DIA,
    JAN,
    FEV,
    MAR,
    ABR,
    MAI,
    JUN,
    JUL,
    AGO,
    SET,
    OUT,
    NOV,
    DEZ;
  }
  
  private enum CAMPOS_CEMADEN {
    MUNI,
    PREFIXO,
    UF,
    NOME_POSTO,
    LAT,
    LONG,
    DATA_HORA,
    VALOR_LEITURA;
  }
  
  public enum CAMPOS_FLU_PRODESP {
    ID, ANO_MES;
  }
  
  public enum CAMPOS_ENCARREGAGOS {
    PREFIXO_POSTO_1,
    PREFIXO_POSTO_2,
    PREFIXO_POSTO_3,
    NOME,
    MUNICIPIO,
    SITUACAO,
    ENCARREGADO;
  }
  private int[] dias_mes = { 
      31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  
  private String linha;
  
  public void uploadFileTelefonada(String path, String tipo) {
    String csvFile = path;

    
    try {
      Scanner scanner = new Scanner(new File(csvFile));
      
      while (scanner.hasNext()) {
        if (tipo == "PLU") {
          readLine(scanner.nextLine()); continue;
        }  if (tipo == "FLU") {
          readLineTelefonadaFlu(scanner.nextLine()); continue;
        } 
        System.out.println("Tipo incorreto");
        
        break;
      } 
      
      scanner.close();
    } catch (FileNotFoundException e) {
      
      e.printStackTrace();
    } 
  }



  
  public void uploadFileCemadenPlu(String path) {
    String csvFile = path;

    
    try {
      Scanner scanner = new Scanner(new File(csvFile));
      
      while (scanner.hasNext()) {
        readLineCemadenPlu(scanner.next());
      }
      scanner.close();
      System.out.println("Done");
    } catch (FileNotFoundException e) {
      
      e.printStackTrace();
    } 
  }




  
  public void uploadFileProdespFlu(String path) {
    try {
      BufferedReader objReader = new BufferedReader(new FileReader(path));
      int count = 0; String strCurrentLine;
      while ((strCurrentLine = objReader.readLine()) != null) {
        readLineProdespFlu(strCurrentLine);
        if (count >= 100000) {
          break;
        }
        count++;
      }
    
    }
    catch (IOException e) {
      
      e.printStackTrace();
    } 
  }



  
  public void uploadFileProdespPlu(String path) {
    try {
      BufferedReader objReader = new BufferedReader(new FileReader(path));
      int count = 0; String strCurrentLine;
      while ((strCurrentLine = objReader.readLine()) != null) {
        readLineProdespPlu(strCurrentLine);
        if (count >= 100000) {
          break;
        }
        count++;
      }
    
    }
    catch (IOException e) {
      
      e.printStackTrace();
    } 
  }

 
  
  private void readLineProdespFlu(String linha) {
    String[] campos = linha.split(";");
    
    String posto = campos[CAMPOS_FLU_PRODESP.ID.ordinal()];
    String ano_mes = campos[CAMPOS_FLU_PRODESP.ANO_MES.ordinal()];
    List<String> valores = new ArrayList<String>();
    for (int i = 2; i < 95; i++) {
      valores.add(campos[i]);
    }






    
    if (Main.MEDICOES_PRODESP.containsKey(posto)) {
      HashMap<String, List<String>> aux = (HashMap)Main.MEDICOES_PRODESP.get(posto);
      aux.put(ano_mes, valores);
      Main.MEDICOES_PRODESP.put(posto, aux);
    } else {
      HashMap<String, List<String>> aux = new HashMap<String, List<String>>();
      aux.put(ano_mes, valores);
      Main.MEDICOES_PRODESP.put(posto, aux);
    } 
  }














  
  private void readLineProdespPlu(String linha) {
    String[] campos = linha.split(";");
    
    String posto = campos[CAMPOS_FLU_PRODESP.ID.ordinal()];
    String ano_mes = campos[CAMPOS_FLU_PRODESP.ANO_MES.ordinal()];
    List<String> valores = new ArrayList<String>();
    for (int i = 2; i < 33; i++) {
      valores.add(campos[i].trim());
    }






    
    if (Main.MEDICOES_PRODESP.containsKey(posto)) {
      HashMap<String, List<String>> aux = (HashMap)Main.MEDICOES_PRODESP.get(posto);
      aux.put(ano_mes, valores);
      Main.MEDICOES_PRODESP.put(posto, aux);
    } else {
      HashMap<String, List<String>> aux = new HashMap<String, List<String>>();
      aux.put(ano_mes, valores);
      Main.MEDICOES_PRODESP.put(posto, aux);
    } 
  }
















  
  public void criarMedicoes() {
//    Calendar today = Calendar.getInstance();
    
    for (String prefixo : Main.MEDICOES_ARQUIVO.keySet()) {
      
      if (Main.POSTOS.containsKey(prefixo)) {
        
        HashMap<String, List<Float>> aux = Main.MEDICOES_ARQUIVO.get(prefixo);
        
        for (String mes : aux.keySet()) {

          
          int year = 2019;
          String month = mes;
          
          List<Float> valores = aux.get(mes);
          
          for (int i = 0; i <= this.dias_mes[Integer.parseInt(mes) - 1] - 1; i++) {

            
            if (Float.compare(((Float)valores.get(i)).floatValue(), 999.99F) != 0) {
              Medicao medicao = new Medicao();
              
              GregorianCalendar cal = new GregorianCalendar(year, Integer.parseInt(month) - 1, i + 1);
              
              TimeZone zone = TimeZone.getTimeZone("America/Sao_Paulo");
  
              String data = "";
              if (zone.inDaylightTime(cal.getTime())) {
                data = String.valueOf(year) + "-" + month + "-" + (i + 1) + " 9:00";
              } else {
                data = String.valueOf(year) + "-" + month + "-" + (i + 1) + " 10:00";
              } 
              
              medicao.setPrefixo_do_posto(prefixo);
              medicao.setPostoId(((Integer)Main.POSTOS.get(prefixo)).intValue());
              medicao.setTipo_classificacao_medicao_id(3);
              medicao.setTipo_transmissao_id(2);
              medicao.setData_hora(data);
              medicao.setValor_leitura(String.valueOf(valores.get(i)));
              medicao.setOrigem_da_informacao("JAVA-TELEFONADA");
              
              if (Main.MEDICOES.containsKey(prefixo)) {
                List<Medicao> temp = Main.MEDICOES.get(prefixo);
                temp.add(medicao);
                Main.MEDICOES.put(prefixo, temp);
              } else {
                List<Medicao> temp = new ArrayList<Medicao>();
                temp.add(medicao);
                Main.MEDICOES.put(prefixo, temp);
              } 
            } 
          } 
        } 


        
        continue;
      } 

      
      Main.PREFIXOS_SEM_ID.add(prefixo);
    } 
  }




  
  private void readLine(String linha) {
    String[] campos = linha.split(";");
    String prefixo = campos[CAMPOS.PREFIXO.ordinal()].replaceAll("\"", "");
    
    HashMap<String, List<Float>> aux = new HashMap<String, List<Float>>();
    
    for (int i = CAMPOS.JAN.ordinal(); i <= CAMPOS.DEZ.ordinal(); i++) {
      
      List<Float> valores = new ArrayList<Float>();
      
      String valor = campos[i];
      if (!valor.equals("-")) {
        valor = valor.replaceAll(",", ".");
        System.out.println(prefixo + " " + valor);
        valores.add(Float.valueOf(Float.parseFloat(valor)));
      } else {
        valor = "999.99";
        valores.add(Float.valueOf(Float.parseFloat(valor)));
      } 
      
      if (Main.MEDICOES_ARQUIVO.containsKey(prefixo)) {
        
        HashMap<String, List<Float>> temp = Main.MEDICOES_ARQUIVO.get(prefixo);
        
        List<Float> valores_atuais = new ArrayList<Float>();
        
        if (temp.containsKey((new StringBuilder(String.valueOf(i - 1))).toString())) {
          
          valores_atuais = temp.get((new StringBuilder(String.valueOf(i - 1))).toString());
          valores_atuais.add(Float.valueOf(Float.parseFloat(valor)));
          temp.put((new StringBuilder(String.valueOf(i - 1))).toString(), valores_atuais);
        }
        else {
          
          valores_atuais.add(Float.valueOf(Float.parseFloat(valor)));
          temp.put((new StringBuilder(String.valueOf(i - 1))).toString(), valores_atuais);
        } 

        
        Main.MEDICOES_ARQUIVO.put(prefixo, temp);
      }
      else {
        
        aux.put((new StringBuilder(String.valueOf(i - 1))).toString(), valores);
        Main.MEDICOES_ARQUIVO.put(prefixo, aux);
      } 
    } 
  }






  
  private void readLineCemadenPlu(String linha) {
    String[] campos = linha.split(";");


    
    int dia_anterior = 0;
    
    String data_hora_atual = String.valueOf(campos[CAMPOS_CEMADEN.DATA_HORA.ordinal()].substring(11, 13)) + campos[CAMPOS_CEMADEN.DATA_HORA.ordinal()].substring(14, 16);
    int dia = Integer.parseInt(campos[CAMPOS_CEMADEN.DATA_HORA.ordinal()].substring(8, 10));
    
    MedicaoGenerica medicao = new MedicaoGenerica();
    
    medicao.setPrefixo_do_posto(campos[CAMPOS_CEMADEN.PREFIXO.ordinal()]);
    medicao.setData_hora(campos[CAMPOS_CEMADEN.DATA_HORA.ordinal()]);
    medicao.setChuva(campos[CAMPOS_CEMADEN.VALOR_LEITURA.ordinal()].replaceAll(",", "."));
    medicao.setChuva_acumulada("0");
    MedicaoGenerica anterior = null;
    
    if (Main.MEDICOES_CEMADEN_PLU.containsKey(medicao.getPrefixo_do_posto())) {
      List<MedicaoGenerica> lista = Main.MEDICOES_CEMADEN_PLU.get(medicao.getPrefixo_do_posto());
      
      if (lista.size() > 0) {
        anterior = (MedicaoGenerica)lista.get(lista.size() - 1);
        
        dia_anterior = Integer.parseInt(anterior.getData_hora().substring(8, 10));
        String data_hora_anterior = String.valueOf(anterior.getData_hora().substring(11, 13)) + anterior.getData_hora().substring(14, 16);
        
        if (dia - dia_anterior == 0) {
          if (Integer.parseInt(data_hora_anterior) < 710 && Integer.parseInt(data_hora_atual) < 710) {
            medicao.setChuva_acumulada(anterior.getChuva_acumulada() + Float.parseFloat(medicao.getChuva()));
          } else if (Integer.parseInt(data_hora_anterior) >= 710 && Integer.parseInt(data_hora_atual) >= 710) {
            medicao.setChuva_acumulada(anterior.getChuva_acumulada() + Float.parseFloat(medicao.getChuva()));
          } else {
            medicao.setChuva_acumulada(medicao.getChuva());
          } 
        } else if (dia - dia_anterior == 1) {
          if (Integer.parseInt(data_hora_anterior) >= 710 && Integer.parseInt(data_hora_atual) < 710) {
            medicao.setChuva_acumulada(anterior.getChuva_acumulada() + Float.parseFloat(medicao.getChuva()));
          } else {
            medicao.setChuva_acumulada(medicao.getChuva());
          } 
        } else {
          medicao.setChuva_acumulada(medicao.getChuva());
        } 
      } 
    } 




    
    if (Main.MEDICOES_CEMADEN_PLU.containsKey(medicao.getPrefixo_do_posto())) {
      List<MedicaoGenerica> aux = (List)Main.MEDICOES_CEMADEN_PLU.get(medicao.getPrefixo_do_posto());
      aux.add(medicao);
      Main.MEDICOES_CEMADEN_PLU.put(medicao.getPrefixo_do_posto(), aux);
    } else {
      List<MedicaoGenerica> aux = new ArrayList<MedicaoGenerica>();
      aux.add(medicao);
      Main.MEDICOES_CEMADEN_PLU.put(medicao.getPrefixo_do_posto(), aux);
    } 
  }














  
  private void readLineTelefonadaFlu(String linha) {
    String[] campos = linha.split(";");
    String prefixo = campos[CAMPOS.PREFIXO.ordinal()].replaceAll("\"", "");
    
    HashMap<String, List<Float>> aux = new HashMap<String, List<Float>>();
    
    for (int i = CAMPOS.JAN.ordinal(); i <= CAMPOS.DEZ.ordinal(); i++) {
      
      List<Float> valores = new ArrayList<Float>();
      
      String valor = campos[i];
      System.out.println(prefixo);
      if (!valor.equals("-")) {
        valor = valor.replaceAll(",", ".");
        System.out.println(String.valueOf(prefixo) + " " + i + " " + valor);
        valores.add(Float.valueOf(Float.parseFloat(valor)));
      } else {
        valor = "999.99";
        valores.add(Float.valueOf(Float.parseFloat(valor)));
      } 
      
      if (Main.MEDICOES_ARQUIVO.containsKey(prefixo)) {
        
        HashMap<String, List<Float>> temp = (HashMap)Main.MEDICOES_ARQUIVO.get(prefixo);
        
        List<Float> valores_atuais = new ArrayList<Float>();
        
        if (temp.containsKey((new StringBuilder(String.valueOf(i - 1))).toString())) {
          
          valores_atuais = (List)temp.get((new StringBuilder(String.valueOf(i - 1))).toString());
          valores_atuais.add(Float.valueOf(Float.parseFloat(valor)));
          temp.put((new StringBuilder(String.valueOf(i - 1))).toString(), valores_atuais);
        }
        else {
          
          valores_atuais.add(Float.valueOf(Float.parseFloat(valor)));
          temp.put((new StringBuilder(String.valueOf(i - 1))).toString(), valores_atuais);
        } 

        
        Main.MEDICOES_ARQUIVO.put(prefixo, temp);
      }
      else {
        
        aux.put((new StringBuilder(String.valueOf(i - 1))).toString(), valores);
        Main.MEDICOES_ARQUIVO.put(prefixo, aux);
      } 
    } 
  }




  
  private String formatarData(String data_hora) {
    String ano = data_hora.substring(0, 4);
    String mes = data_hora.substring(4, 6);
    String dia = data_hora.substring(6, 8);
    String hora = data_hora.substring(8, 10);
    String minutos = data_hora.substring(10, 12);
    
    return new String(String.valueOf(ano) + "-" + mes + "-" + dia + " " + hora + ":" + minutos);
  }
}
