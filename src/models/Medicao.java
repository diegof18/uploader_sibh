package models;



public class Medicao
{
  private String id;
  private String prefixo_do_posto;
  private String codigo_externo_do_posto;
  private String data_hora;
  private String valor;
  private String valor_leitura;
  private String tipo_do_posto;
  private String origem_da_informacao;
  private String tensao_bateria;
  private String created_at;
  private String updated_at;
  private int posto_id;
  private int tipo_transmissao_id;
  private int tipo_classificacao_medicao_id;
  
  public Medicao() { this.tipo_classificacao_medicao_id = 3; }



  
  public Medicao(String id, String prefixo_do_posto, String codigo_externo_do_posto, String data_hora, String valor, String valor_leitura, String tipo_do_posto, String origem_da_informacao, String tensao_bateria, String created_at, String updated_at, int posto_id) {
    this.id = id;
    this.prefixo_do_posto = prefixo_do_posto;
    this.codigo_externo_do_posto = codigo_externo_do_posto;
    this.data_hora = data_hora;
    this.valor = valor;
    this.valor_leitura = valor_leitura;
    this.tipo_do_posto = tipo_do_posto;
    this.origem_da_informacao = origem_da_informacao;
    this.tensao_bateria = tensao_bateria;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.posto_id = posto_id;
    this.tipo_classificacao_medicao_id = 3;
  }
  
  public String getId() { return this.id; }

  
  public void setId(String id) { this.id = id; }

  
  public String getPrefixo_do_posto() { return this.prefixo_do_posto; }

  
  public void setPrefixo_do_posto(String prefixo_do_posto) { this.prefixo_do_posto = prefixo_do_posto; }

  
  public String getCodigo_externo_do_posto() { return this.codigo_externo_do_posto; }

  
  public void setCodigo_externo_do_posto(String codigo_externo_do_posto) { this.codigo_externo_do_posto = codigo_externo_do_posto; }

  
  public String getData_hora() { return this.data_hora; }

  
  public void setData_hora(String data_hora) { this.data_hora = data_hora; }

  
  public String getValor() { return this.valor; }

  
  public void setValor(String valor) { this.valor = valor; }

  
  public String getValor_leitura() { return this.valor_leitura; }

  
  public void setValor_leitura(String valor_leitura) { this.valor_leitura = valor_leitura; }

  
  public String getTipo_do_posto() { return this.tipo_do_posto; }

  
  public void setTipo_do_posto(String tipo_do_posto) { this.tipo_do_posto = tipo_do_posto; }

  
  public String getOrigem_da_informacao() { return this.origem_da_informacao; }

  
  public void setOrigem_da_informacao(String origem_da_informacao) { this.origem_da_informacao = origem_da_informacao; }

  
  public String getTensao_bateria() { return this.tensao_bateria; }

  
  public void setTensao_bateria(String tensao_bateria) { this.tensao_bateria = tensao_bateria; }

  
  public String getCreated_at() { return this.created_at; }

  
  public void setCreated_at(String created_at) { this.created_at = created_at; }

  
  public String getUpdated_at() { return this.updated_at; }

  
  public void setUpdated_at(String updated_at) { this.updated_at = updated_at; }

  
  public int getPostoId() { return this.posto_id; }


  
  public void setPostoId(int posto_id) { this.posto_id = posto_id; }


  
  public int getTipo_transmissao_id() { return this.tipo_transmissao_id; }

  
  public void setTipo_transmissao_id(int tipo_transmissao_id) { this.tipo_transmissao_id = tipo_transmissao_id; }

  
  public int getTipo_classificacao_medicao_id() { return this.tipo_classificacao_medicao_id; }

  
  public void setTipo_classificacao_medicao_id(int tipo_classificacao_medicao_id) { this.tipo_classificacao_medicao_id = tipo_classificacao_medicao_id; }
}
